<?php

function safeclick_admin_form() {
  $form = array();

  $options = _safeclick_get_options();

  $form['safeclick'] = array(
    '#type' => 'fieldset',
    '#title' => t('Protection levels'),
    '#tree' => TRUE,
    '#description' => t('Configure levels and various techniques of protection from Clickjacking attacks'),
  );

  $x_frame_options = array(
    SAFECLICK_X_FRAME_DISABLE => t('Disabled'),
    SAFECLICK_X_FRAME_SAMEORIGIN => t('SAMEORIGIN'),
    SAFECLICK_X_FRAME_NONE => t('DENY'),
  );

  $form['safeclick']['x_frame'] = array(
    '#type' => 'select',
    '#title' => t('X-Frame-Options'),
    '#options' => $x_frame_options,
    '#default_value' => $options['x_frame'],
    '#description' => t('<div>Configure X-Frame-Options HTTP header. Currently, it is supported by Mozilla Firefox + NoScript, Apple Safari, Google Chrome and Internet Explorer 8.</div><div>If set to <b>SAMEORIGIN</b>, website accepts all attempts of framing within its domain. Enabled by default.</div><div>If set to <b>DENY</b>, website rejects any attempt of framing.</div>'),
  );

  $form['safeclick']['js_css_protection'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable JavaScript and CSS protection'),
    '#return_value' => 1,
    '#default_value' => $options['js_css_protection'],
    '#description' => t('<div>Enable protection via JavaScript and CSS.</div><div>This is the most efficient Clickjacking prevention technique.</div><div>Thanks to <a href="http://www.sirdarckcat.net/">sirdarckcat</a> for showing this trick.</div><div style="color: red"><b>WARNING!</b> It may break modules, which use iFrames.</div><div>If you experience any problems after enabling this option, please, <a href="http://drupal.org/project/issues/safeclick">report</a> issues.</div>'),
  );

  $form['safeclick']['no_script'] = array(
    '#type' => 'checkbox',
    '#title' => t('Promt to turn on JavaScript'),
    '#return_value' => 1,
    '#default_value' => $options['no_script'],
    '#description' => t('<div>Add &lt;noscript&gt; tag to prevent surfing of website with disabled JavaScript.</div><div>Special message will be shown to user.</div><div>This option should be used along with JavaScript and CSS protection.</div><div>If this option is not enabled and JavaScript + CSS is enabled, users with disabled JavaScript will see blank screen instead of website.</div>'),
  );

  $form['safeclick']['override_style'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override styles for iFrames'),
    '#return_value' => 1,
    '#default_value' => $options['override_style'],
    '#description' => t('<div>Enable this option only if you allow users to post &lt;iframe&gt;, &lt;frame&gt;, &lt;object&gt; or &lt;embed&gt; tags.</div><div>Most Clickjacking vectors use transparent iFrames and this stylesheet overrides opacity of iframe, frame, object and embed HTML elements to make them be always shown and also prevents "last loaded - first focused" behavior (when last loaded loaded iFrame is focused regardless its z-index).</div><div>Please, note that this method is not 100% proof.</div><div style="color: red"><b>WARNING!</b> It may break layout of particular stated tags.</div><div>If enabling this options conflicts with any contributed module, please, <a href="http://drupal.org/project/issues/safeclick">report</a> issues.</div>'),
  );
  
  return system_settings_form($form);
}
