
Description
-----------
SafeClick module provides Drupal with various techniques and methods
of protection from Clickjacking attacks.

Installation
------------
Extract archive with module to contributed modules directory.
Enable it on Site Building -> Modules page.
Settings of module are available on http://www.example.com/admin/settings/safeclick

Features
--------
More information about features can be found on project page
http://drupal.org/project/safeclick

Issues
------
If you experience any problems with SafeClick, please,
report issues on http://drupal.org/project/issues/safeclick

Author
------
Alex Rodionov
p0deje [at] gmail [dot] com
http://p0deje.blogspot.com
http://drupal.org/user/529960
